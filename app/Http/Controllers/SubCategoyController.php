<?php

namespace App\Http\Controllers;

use App\Helpers\JsonResponse;
use App\Http\Requests\SubCategory\StoreSubCategoryRequest;
use App\Http\Requests\SubCategory\UpdateSubCategoryRequest;
use App\Http\Resources\SubCategoryResource;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SubCategoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = SubCategory::latest()->get();
        return JsonResponse::success(SubCategoryResource::collection($categories), 200, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubCategoryRequest $request)
    {
        $input = $request->all();

        //hash image name and store in public storage
        $filename = null;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->hashName();
            $file->storeAs('public/images/subCategory_images/', $filename);
        }
        $input['image'] = $filename;
        $sub_category = SubCategory::create($input);
        return JsonResponse::success(new SubCategoryResource($sub_category), 201, JsonResponse::MSG_ADDED_SUCCESSFULLY);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        return JsonResponse::success(new SubCategoryResource($subCategory), 201, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubCategoryRequest $request, SubCategory $subCategory)
    {
        $input = $request->all();

        $filename = null;
        if ($request->hasFile('image')) {
            //remove old image
            if ($subCategory->image && Storage::exists('public/images/subCategory_images/' . $subCategory->image)) {
                Storage::delete('public/images/subCategory_images/' . $subCategory->image);
            }

            $file = $request->file('image');
            $filename = $file->hashName();
            $file->storeAs('public/images/subCategory_images/', $filename);
            $input['image'] = $filename;
        }

        $subCategory->update($input);
        return JsonResponse::success(null, 200, JsonResponse::MSG_UPDATED_SUCCESSFULLY);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();
        return JsonResponse::success([], 200, JsonResponse::MSG_DELETED_SUCCESSFULLY);
    }
}
