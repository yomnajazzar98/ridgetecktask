<?php

namespace App\Http\Controllers;

use App\Helpers\JsonResponse;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        return JsonResponse::success(CategoryResource::collection($categories), 200, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $input = $request->all();

        //hash image name and store in public storage
        $filename = null;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->hashName();
            $file->storeAs('public/images/category_images/', $filename);
        }
        $input['image'] = $filename;
        $category = Category::create($input);
        return JsonResponse::success(new CategoryResource($category), 201, JsonResponse::MSG_ADDED_SUCCESSFULLY);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        // if (!$category) {
        //     return JsonResponse::error(404,JsonResponse::MSG_NOT_FOUND);
        // }
        return JsonResponse::success(new CategoryResource($category), 201, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        // if (!$category) {
        //     return JsonResponse::error(404,JsonResponse::MSG_NOT_FOUND);
        // }

        $input = $request->all();

        $filename = null;
        if ($request->hasFile('image')) {
            //remove old image
            if ($category->image && Storage::exists('public/images/category_images/' . $category->image)) {
                Storage::delete('public/images/category_images/' . $category->image);
            }
            $file = $request->file('image');
            $filename = $file->hashName();
            $file->storeAs('public/images/category_images/', $filename);
            $input['image'] = $filename;
        }
        $category->update($input);
        return JsonResponse::success(null, 200, JsonResponse::MSG_UPDATED_SUCCESSFULLY);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // if (!$category) {
        //     return JsonResponse::error(404,JsonResponse::MSG_NOT_FOUND);
        // }
        $category->delete();
        return JsonResponse::success([], 200, JsonResponse::MSG_DELETED_SUCCESSFULLY);
    }
}
