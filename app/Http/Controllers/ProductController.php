<?php

namespace App\Http\Controllers;

use App\Helpers\JsonResponse;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->get();
        return JsonResponse::success(ProductResource::collection($products), 200, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = Product::create($request->all());
        if ($images = $request->images) {
            foreach ($images as $image) {
                $product->addMedia($image)->toMediaCollection('images/product-images');
            }
        }
        return JsonResponse::success(new ProductResource($product), 201, JsonResponse::MSG_ADDED_SUCCESSFULLY);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return JsonResponse::success(new ProductResource($product), 201, JsonResponse::MSG_SUCCESS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update($request->all());
        if ($images = $request->images) {
            $product->clearMediaCollection('images/product-images');
            foreach ($images as $image) {
                $product->addMedia($image)->toMediaCollection('images/product-images');
            }
        }
        return JsonResponse::success(null, 200, JsonResponse::MSG_UPDATED_SUCCESSFULLY);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return JsonResponse::success([], 200, JsonResponse::MSG_DELETED_SUCCESSFULLY);
    }
}
