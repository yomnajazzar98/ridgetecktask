<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'price' => ['required', 'numeric', 'min:0'],
            'images' => ['required', 'array'],
            'images.*' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'sub_category_id' => ['required' , 'integer','exists:sub_categories,id'],
        ];
    }
}
