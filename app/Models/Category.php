<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'image',
    ];
    public function getImagePathAttribute()
    {
        return asset('storage/images/category_images/' . $this->image);
    }
    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class, 'category_id');
    }
}
