<?php

namespace App\Helpers;

class JsonResponse
{
    const MSG_ADDED_SUCCESSFULLY = 'msg_added_successfully';
    const MSG_UPDATED_SUCCESSFULLY = "msg_updated_successfully";
    const MSG_DELETED_SUCCESSFULLY = "msg_deleted_successfully";
    const MSG_SUCCESS = "msg_success";
    const MSG_NOT_FOUND = "msg_not_found";

    /**
     * @param array $data
     * @param String $message
     * @param int $code
     * @param array|null $pagination
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($data = null, $code, $message, $pagination = null)
    {
        return response()->json([
            'message' => trans($message),
            'data' => $data,
            'pagination' => $pagination,
            'status' => $code == 200 || $code == 201 || $code == 204
        ], $code);
    }

    /**
     * @param $code
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($code, $message)
    {
        return response()->json([
            'message' => trans($message),
            'status' => false
        ], $code);
    }
}
